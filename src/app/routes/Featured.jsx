import React from 'react';
import Article from '../components/Article.jsx';

export default class Featured extends React.Component {

  constructor(props) {
    super(props);
    this.data = null;
  }

  parseArticleData(data) {
    this.data = data;
  }

  render() {
    const Articles = this.props.data.map((article, i) => <Article key={i} title={article.title} author={article.author} date={article.date} poster={article.poster} />);

    return (
      <div class="ghost__featured ghost__flex">
        {Articles}
      </div>
    );
  }
}
