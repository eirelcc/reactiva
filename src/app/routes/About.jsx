import React from 'react';

export default class About extends React.Component {
  // static contextTypes = {
  //   router: React.PropTypes.func.isRequired
  // };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="ghost__about">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex similique nulla numquam, quidem autem quam quos harum eveniet velit molestiae, iusto totam, nobis fugit ullam laborum voluptatum earum explicabo vitae suscipit minus quibusdam dolorem, voluptas architecto molestias. Quasi, a. Laboriosam earum, unde soluta eligendi facilis temporibus, ullam minus ratione quae nostrum, vel possimus quia eveniet tenetur molestiae deserunt quas nulla quasi velit doloribus porro nobis. Maxime facere soluta ut animi, autem odio, libero! Sit minima quis molestias laudantium repellendus vero architecto numquam aspernatur nulla, doloribus consectetur provident accusamus placeat minus rem perferendis ducimus ratione totam possimus sequi quod! Rerum deleniti quas sit repellendus consequuntur, error ut aperiam adipisci cum, autem laudantium pariatur atque. Placeat, inventore? Excepturi saepe incidunt perferendis sapiente modi, quas eum eligendi, dolorum ea alias libero tempora atque, at beatae. Repellendus ut, sit corporis sint ad, expedita assumenda est odit? Perferendis, aspernatur earum rerum odit cupiditate sapiente voluptates aliquam blanditiis doloribus assumenda deserunt, numquam provident nisi accusantium, saepe impedit ipsam corrupti, natus illo quos! Ut voluptas totam animi odio magni dolorem ab eaque aperiam nemo. Cum fugit odio cupiditate iure fuga vitae, accusantium dolore aut nam adipisci labore magni, libero aperiam voluptas voluptatibus non cumque porro quibusdam animi!</p>
      </div>
    );
  }
}
