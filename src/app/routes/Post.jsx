import React from 'react';
import { Link } from 'react-router';

export default class Post extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const onArticleClick = function() {
      console.log(this.props)
    };

    const image = {
      backgroundImage: 'url(http://east.aspirethemes.com/content/images/2016/02/photo-1444760134166-9b8f7d0fc038.jpg)'
    };

    const title = 'Everybody listen! We have to put a barrier between us and the world!';
    const date = '22 December 2015';

    return (
      <div class="ghost__post" >
        <div className="post__img" style={image}></div>
        <article className="post__article">
          <div className="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, illo?</div>
          <time class="post__date" >22 December 2015</time>
          <div className="post__body">

            <p>Lorem ipsum dolor sit amet, vix ut case porro facilisis, alia possit neglegentur vis te. Has cu eirmod abhorreant, vel civibus efficiantur cu. Eu summo elitr vix, iusto putant maluisset per ut, ne etiam vivendum adipisci vel. Vis omnis tempor accusam ei, justo perpetua liberavisse cu qui. Saperet aliquando adipiscing ius ne, ne facer euripidis est. Pro mundi nostrum suavitate et.</p>

            <p>Vix dico eius nominati cu, ex zril commodo fuisset mea. Habeo adhuc audiam ius no. In quis virtute officiis has. Vix ad honestatis accommodare, quis virtute et sit, pertinax expetenda eam id. Duo an fuisset delectus dissentias, justo altera ea per.</p>

            <h3>Todo</h3>

            <p>Duis id ante elit. Aliquam quis tellus id orci eleifend finibus. Donec consequat justo ligula, eget sodales purus hendrerit at.</p>

            <ol>
              <li>Ut at interdum nunc. Maecenas commodo turpis quis elementum gravida.</li>
              <li>Nunc ac sapien tellus. Quisque risus enim, tempus eget porttitor in, pellentesque vel urna. <br/>Donec nibh massa, rutrum a sollicitudin eu, lacinia in lorem.</li>
            </ol>

            <h3>Graphic design</h3>
            <blockquote>
              <p>Graphic design is the paradise of individuality, eccentricity, heresy, abnormality, hobbies, and humors. — George Santayana</p>
            </blockquote>

            <p>Vim te case nihil oblique, has partem interpretaris ne, ad admodum accusamus nam. Usu utinam dissentias referrentur ne, vim accusam voluptua pertinacia no. Est no posse utinam inermis, brute errem mentitum et ius, te prompta albucius quo. In pro simul soleat regione.</p>

            <p>
              <img src="https://images.unsplash.com/photo-1427805371062-cacdd21273f1?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=2d169a7f9dc7f4c695be56969dd255cf" alt=""/>
            </p>

            <p>Ne reque offendit singulis mea, ad eos ferri doming nostrud. Duis suscipit usu ut, fuisset pericula ex est, et porro prompta his. Audire definiebas voluptatibus et duo, aperiam ocurreret ad nec. Vel ad nostrud principes. Ad liber congue iracundia sed, eirmod erroribus eam te, has veniam epicurei ea.</p>

            <p>Pri probo alterum aliquando an. Duo appetere laboramus intellegat ea, ex suas diam exerci vix. Mel simul debitis id, est nusquam fuisset mentitum in. Te mei iudico iisque.</p>

            <div className="post__tagline">
              <a href="#" className="post__tag">Travel</a>
              <a href="#" className="post__tag">lifestyle</a>
            </div>

            <hr/>

            <div className="post__author">
              <div className="post__flex">
                  <div className="author__img">
                      <div className="img" ></div>
                  </div>
                  <div className="author__aside">
                    <a href="#" className="author__title">John Adam</a>
                    <div className="author__about">Adam have over 5 years of experience as a web writer, and also a photographer, his work appears on popular web magazines. He is the author of AspireThemes</div>
                    <div className="author__meta">
                      <div className="author__locale">
                        <svg class="icon__locale" viewBox="0 0 50 50">
                          <path d="M25 42.5l-.8-.9C23.7 41.1 12 27.3 12 19c0-7.2 5.8-13 13-13s13 5.8 13 13c0 8.3-11.7 22.1-12.2 22.7l-.8.8zM25 8c-6.1 0-11 4.9-11 11 0 6.4 8.4 17.2 11 20.4 2.6-3.2 11-14 11-20.4 0-6.1-4.9-11-11-11z"></path>
                            <path d="M25 24c-2.8 0-5-2.2-5-5s2.2-5 5-5 5 2.2 5 5-2.2 5-5 5zm0-8c-1.7 0-3 1.3-3 3s1.3 3 3 3 3-1.3 3-3-1.3-3-3-3z"></path>
                        </svg>
                        <span>London - UK</span>
                      </div>
                      <div className="author__link">
                      <svg id="ei-link-icon" className="icon__locale" viewBox="0 0 50 50"><path d="M24 30.2c0 .2.1.5.1.8 0 1.4-.5 2.6-1.5 3.6l-2 2c-1 1-2.2 1.5-3.6 1.5-2.8 0-5.1-2.3-5.1-5.1 0-1.4.5-2.6 1.5-3.6l2-2c1-1 2.2-1.5 3.6-1.5.3 0 .5 0 .8.1l1.5-1.5c-.7-.3-1.5-.4-2.3-.4-1.9 0-3.6.7-4.9 2l-2 2c-1.3 1.3-2 3-2 4.9 0 3.8 3.1 6.9 6.9 6.9 1.9 0 3.6-.7 4.9-2l2-2c1.3-1.3 2-3 2-4.9 0-.8-.1-1.6-.4-2.3L24 30.2z"></path><path d="M33 10.1c-1.9 0-3.6.7-4.9 2l-2 2c-1.3 1.3-2 3-2 4.9 0 .8.1 1.6.4 2.3l1.5-1.5c0-.2-.1-.5-.1-.8 0-1.4.5-2.6 1.5-3.6l2-2c1-1 2.2-1.5 3.6-1.5 2.8 0 5.1 2.3 5.1 5.1 0 1.4-.5 2.6-1.5 3.6l-2 2c-1 1-2.2 1.5-3.6 1.5-.3 0-.5 0-.8-.1l-1.5 1.5c.7.3 1.5.4 2.3.4 1.9 0 3.6-.7 4.9-2l2-2c1.3-1.3 2-3 2-4.9 0-3.8-3.1-6.9-6.9-6.9z"></path><path d="M20 31c-.3 0-.5-.1-.7-.3-.4-.4-.4-1 0-1.4l10-10c.4-.4 1-.4 1.4 0s.4 1 0 1.4l-10 10c-.2.2-.4.3-.7.3z"></path></svg>
                      <a href="http://aspirethemes.com/">http://aspirethemes.com/</a>
                      </div>
                    </div>
                    
                  </div>
              </div>
            </div>

            <ul className="post__share">
              <li>
                <a href="#">
                  <svg class="icon--tw" viewBox="0 0 50 50">
                    <path d="M39.2 16.8c-1.1.5-2.2.8-3.5 1 1.2-.8 2.2-1.9 2.7-3.3-1.2.7-2.5 1.2-3.8 1.5-1.1-1.2-2.7-1.9-4.4-1.9-3.3 0-6.1 2.7-6.1 6.1 0 .5.1.9.2 1.4-5-.2-9.5-2.7-12.5-6.3-.5.7-.8 1.7-.8 2.8 0 2.1 1.1 4 2.7 5-1 0-1.9-.3-2.7-.8v.1c0 2.9 2.1 5.4 4.9 5.9-.5.1-1 .2-1.6.2-.4 0-.8 0-1.1-.1.8 2.4 3 4.2 5.7 4.2-2.1 1.6-4.7 2.6-7.5 2.6-.5 0-1 0-1.4-.1 2.4 1.9 5.6 2.9 9 2.9 11.1 0 17.2-9.2 17.2-17.2V20c1.2-.9 2.2-1.9 3-3.2z"></path>
                  </svg>
                </a>
              </li>
              <li>
                <a href="#">
                  <svg class="icon--fb" viewBox="0 0 50 50">
                    <path d="M26 20v-3c0-1.3.3-2 2.4-2H31v-5h-4c-5 0-7 3.3-7 7v3h-4v5h4v15h6V25h4.4l.6-5h-5z"></path>
                  </svg>
                </a>
              </li>
              <li>
                <a href="#">
                  <svg class="icon--gplus" viewBox="0 0 50 50"><path d="M18 23v4.8h7.9c-.3 2.1-2.4 6-7.9 6-4.8 0-8.7-4-8.7-8.8s3.9-8.8 8.7-8.8c2.7 0 4.5 1.2 5.6 2.2l3.8-3.7C24.9 12.4 21.8 11 18 11c-7.7 0-14 6.3-14 14s6.3 14 14 14c8.1 0 13.4-5.7 13.4-13.7 0-.9-.1-1.6-.2-2.3H18z"></path><path d="M48 23h-4v-4h-4v4h-4v4h4v4h4v-4h4z"></path></svg>
                </a>
              </li>
            </ul>

            <div className="post__prev">
              <a href="#">
                <div className="prev__img">
                  <span>Previous Story</span>
                </div>
                <div className="prev__author"></div>
                <div className="prev__date"></div>
                <div className="prev__title">If you have an opportunity to use your voice you should use it all</div>
              </a>
            </div>
            <div className="separator">
              <span className="title">NEWSLETTER</span>
            </div>

            <div className="box box--news-letter">
              <form action="search" method="post" class="">
                <input type="email" name="email" className="input__solid" placeholder="Your email address" required/>
                <button type="submit" class="btn btn--dark">SIGN UP</button>
              </form>
            </div>

            <div className="separator">
              <span className="title">COMMENTS</span>
            </div>
          </div>
        </article>
      </div>
    );
  }
}
