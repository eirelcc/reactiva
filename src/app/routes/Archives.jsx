import React from 'react';

export default class Archives extends React.Component {
  // static contextTypes = {
  //   router: React.PropTypes.func.isRequired
  // };

  constructor(props) {
    super(props);
  }

  render() {
    const { query } = this.props.location;
    const { params } = this.props;
    const { article } = params;
    const { date, filter } = query;
    return (
      <div class="ghost__archives">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam hic perferendis quos expedita, adipisci sed. Labore, voluPptate harum fugiat ad minus laborum rerum ea praesentium consequuntur quas aut cum officiis sed, voluptatibus consequatur vel ut repellat commodi! Voluptatum nam aut omnis molestias, delectus dolorem doloribus explicabo, nobis voluptas suscipit sequi perferendis aliquam quos, officia, tempora! Officia sapiente, porro, nulla, beatae odio quaerat ducimus natus deserunt non ad fuga. Commodi iusto, blanditiis odit perferendis dignissimos doloribus eos, omnis tempora? Libero magnam vero labore quaerat a doloribus corrupti, veritatis cupiditate est, excepturi consectetur iste repellendus laboriosam quas, fugiat, consequuntur cum. Voluptatum quis, libero animi saepe eos vitae. Voluptates perferendis quis, accusamus adipisci labore quae qui, ad. Eaque maxime excepturi doloremque saepe! Quia debitis rerum similique dicta, corporis voluptate quisquam, odio deleniti unde a, quod dolore. Laudantium minima dolor harum consequuntur! Ut harum unde, obcaecati sed ipsam, eum atque consequatur itaque a impedit soluta ad sint asperiores repudiandae doloremque reiciendis nam. Ab aut deserunt similique a rerum dignissimos quisquam modi vel temporibus quasi. Nisi illum, accusantium doloribus, eius quis accusamus fugiat reiciendis temporibus distinctio earum doloremque optio culpa aperiam quas iusto alias voluptatem maiores perferendis recusandae tenetur impedit delectus! Voluptate eum quaerat velit sapiente suscipit ea officiis facilis adipisci dignissimos ab facere quibusdam impedit eveniet porro fugit consectetur eaque id dolorum quis molestias, molestiae libero reiciendis. Harum animi minima sequi, ullam, optio sit natus nisi eaque eligendi accusantium cum alias repudiandae, doloremque magni nobis reiciendis. Provident delectus eius est tempora praesentium, incidunt maiores pariatur, consequatur velit repellendus ex eveniet atque sequi inventore iste nesciunt modi non culpa voluptate explicabo ipsum dolorem! Eum asperiores ipsam facilis recusandae. Nisi, ut aut quam, officia vero nobis cupiditate excepturi qui quod atque tempore. Maiores ratione error cum veritatis dolores id. Ipsa iusto perspiciatis rem cum, esse unde dolor ut neque eligendi dolorum repudiandae et eos ipsum molestias nihil accusamus numquam officiis! Quo incidunt consequatur eaque, totam laborum veritatis nobis rem eos esse cupiditate illum iure eligendi alias non praesentium omnis quaerat distinctio tempora, est ratione quos. Laboriosam, voluptatibus sapiente. Quas voluptas culpa natus vitae! Veniam eum, corrupti rem laudantium necessitatibus dicta, tenetur, incidunt deserunt quibusdam nemo similique non in reiciendis ipsum veritatis neque! Ut dolor, omnis odio fugiat, aspernatur qui obcaecati accusamus totam aliquam earum, delectus quae! Ratione reiciendis totam rerum corrupti ducimus sapiente cupiditate perferendis, delectus, nulla repellendus a. Nisi velit molestias voluptatem autem distinctio, perspiciatis ducimus, quos corporis odio? Iure amet animi nobis dolorum laboriosam voluptatibus doloribus mollitia officiis, magni unde blanditiis accusamus, ipsam ratione. Deleniti consequatur possimus minima expedita praesentium ullam rem, dolorem magnam tempora, minus vitae eos sint itaque numquam in tenetur. Vitae optio et repellendus temporibus dolor totam enim quaerat pariatur doloremque quasi molestiae, neque iste modi ipsa fuga earum accusantium labore laborum iusto aliquam ut rerum. Consequuntur reiciendis, numquam nam nobis animi totam iusto. Impedit atque doloremque sit, dolore incidunt minus cumque eos obcaecati repellat, provident eaque architecto autem doloribus totam ipsa ipsam veritatis, a sunt excepturi nam. Exercitationem illum, dolorem.</p>
      </div>
    );
  }
}
