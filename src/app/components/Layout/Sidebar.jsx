import React from "react";
import Header from "./Header.jsx";
import Footer from "./Footer.jsx";

import { Link } from 'react-router';

export default class Sidebar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="ghost__sidebar">
        <Header></Header>

        <div className="ghost__flex--vertical">
          <div class="ghost__container">
            <div className="ghost__logo"><Link to="/">Ghost</Link></div>
            <div className="ghost__description">Yet another reactive blog</div>
            <nav className="ghost__nav">
              <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="archives">Archives</Link></li>
                <li><Link to="about">About</Link></li>
                <li><Link to="featured">RSS</Link></li>
              </ul>
            </nav>
            <ul className="ghost__social">
              <li>
                <Link to="/" className="ghost__icon--tw">
                  <svg class="icon__cnt" viewBox="0 0 50 50">
                    <path d="M39.2 16.8c-1.1.5-2.2.8-3.5 1 1.2-.8 2.2-1.9 2.7-3.3-1.2.7-2.5 1.2-3.8 1.5-1.1-1.2-2.7-1.9-4.4-1.9-3.3 0-6.1 2.7-6.1 6.1 0 .5.1.9.2 1.4-5-.2-9.5-2.7-12.5-6.3-.5.7-.8 1.7-.8 2.8 0 2.1 1.1 4 2.7 5-1 0-1.9-.3-2.7-.8v.1c0 2.9 2.1 5.4 4.9 5.9-.5.1-1 .2-1.6.2-.4 0-.8 0-1.1-.1.8 2.4 3 4.2 5.7 4.2-2.1 1.6-4.7 2.6-7.5 2.6-.5 0-1 0-1.4-.1 2.4 1.9 5.6 2.9 9 2.9 11.1 0 17.2-9.2 17.2-17.2V20c1.2-.9 2.2-1.9 3-3.2z"></path>
                  </svg>
                </Link>
              </li>
              <li>
                <Link to="/" className="ghost__icon--fb">
                  <svg class="icon__cnt" viewBox="0 0 50 50">
                    <path d="M26 20v-3c0-1.3.3-2 2.4-2H31v-5h-4c-5 0-7 3.3-7 7v3h-4v5h4v15h6V25h4.4l.6-5h-5z"></path>
                  </svg>
                </Link>
              </li>
              <li>
                <Link to="/" className="ghost__icon--ins">
                  <svg class="icon__cnt" viewBox="0 0 50 50">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M35 23h-2.3c.2.6.3 1.3.3 2 0 4.4-3.6 8-8 8s-8-3.6-8-8c0-.7.1-1.4.3-2H15v11c0 .6.4 1 1 1h18c.6 0 1-.4 1-1V23zm0-7c0-.6-.4-1-1-1h-3c-.6 0-1 .4-1 1v3c0 .6.4 1 1 1h3c.6 0 1-.4 1-1v-3zm-10 4c-2.8 0-5 2.2-5 5s2.2 5 5 5 5-2.2 5-5-2.2-5-5-5m10 18H15c-1.7 0-3-1.3-3-3V15c0-1.7 1.3-3 3-3h20c1.7 0 3 1.3 3 3v20c0 1.7-1.3 3-3 3"></path>
                  </svg>
                </Link>
              </li>
              <li>
                <Link to="/" className="ghost__icon--search">
                  <svg class="icon__cnt" viewBox="0 0 50 50">
                    <path d="M23 36c-7.2 0-13-5.8-13-13s5.8-13 13-13 13 5.8 13 13-5.8 13-13 13zm0-24c-6.1 0-11 4.9-11 11s4.9 11 11 11 11-4.9 11-11-4.9-11-11-11z"></path>
                    <path d="M32.682 31.267l8.98 8.98-1.414 1.414-8.98-8.98z"></path>
                  </svg>
                </Link>
              </li>
            </ul>
          </div>
        </div>

        <Footer></Footer>
      </div>
    );
  }
}