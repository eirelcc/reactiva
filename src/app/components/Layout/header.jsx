import React from "react";
import { Link } from 'react-router';

export default class Header extends React.Component {
  
  constructor(props) {
    super(props);
  }

  render() {
  
    return (
      <header className="brand__header">
        <div className="flex--between">
          <div className="brand__slash">
            <svg x="0px" y="0px" viewBox="0 0 95.3 44.3" enable-background="new 0 0 95.3 44.3">
              <path fill="none" stroke="#FFFFFF" strokeWidth="3" d="M32.3,8.8L3.5,22.6l28.6,13.3"/>
              <path fill="none" stroke="#FFFFFF" strokeWidth="3" d="M63,35.6l28.7-13.7L63.2,8.6"/>
              <path fill="none" stroke="#FFFFFF" strokeWidth="3" d="M55.9,0.5L39.3,43.7"/>
            </svg>
          </div>
          <div>
            <div className="btn--down"></div>
            <div className="lang-group">
              <span data-lang="ru">РУС</span>
              <span data-lang="en">EN</span>
            </div>
        </div>
        </div>
      </header>
    );
  }
}