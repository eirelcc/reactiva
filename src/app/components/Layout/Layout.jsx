import React from "react";

import "./styles/style.styl";
import Header from "./header.jsx";
import Footer from "./header.jsx";

export default class Layout extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header></Header>
        <div className="brand__container">
            
            <section className="brand__section">
              <div className="flex--center">
                  <div className="brand__logo"></div> <br/>
                  <div className="brand__description"><span>W</span>e develop/ <span>W</span>e create/  <span>W</span>e make</div>
                  <span className="btn btn--order">СДЕЛАТЬ ЗАКАЗ</span>
              </div>
            </section>

            <section className="brand__section">
              <div className="brand__wrapper">
                <div className="brand__title"><span>К</span>оманда</div>
              </div>
              <div className="grid grid--team">
                <div className="flex">
                  <div className="item"></div>
                </div>
              </div>
            </section>

            <section className="brand__section">
            </section>

            {/* {this.props.children} */}
        </div>
        <Footer></Footer>
      </div>
    );
  }
}


