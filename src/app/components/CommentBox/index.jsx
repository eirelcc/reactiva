import React from "react";
import "./styles/style.styl";

let data = [
  {id: 1, author: "Pete Hunt", text: "This is one comment"},
  {id: 2, author: "Jordan Walke", text: "This is *2nd* comment"},
  {id: 3, author: "John Doe", text: "This is *3rd* comment"}
];

export default class CommentBox extends React.Component {
  
  static contextTypes = {
    router: React.PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.name = 'CommentBox'
  }

  getInitialState() {
    return {data: []};
  }

  render() {
    return (
      <div className="commentBox">
        <h1>{this.name}</h1>
        <CommentList data={this.props.data} />
        <CommentForm />
      </div>
    );
  }
}

class CommentList extends React.Component {
  constructor() {
    super();
  }

  render() {
     this.commentNodes = this.props.data.map(function(comment) {
        return (
          <Comment author={comment.author} key={comment.id}>
            {comment.text}
          </Comment>
        );
      });
    return (
      <div className="commentList">
        {this.commentNodes}
      </div>
    )
  }
}


class CommentForm extends React.Component {
  render() {
    return (
      <div className="commentForm">
      </div>
    );
  }
}

class Comment extends React.Component {
  render() {
    return (
      <div className="comment">

        <h1 className="commentAuthor">
            {this.props.author}
        </h1>
        {this.props.children}
      </div>
    );
  }
}