import React from "react";
import { Router, Route, Link } from 'react-router'

export default class Article extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const poster = {
      backgroundImage: 'url('+ this.props.poster + ')'
    }

    return (
      <article className="ghost__article">
        <Link to="post" params={{ title: "hello" }}>
          <div className="article__img" style={poster}></div>
        </Link>
        <div className="article__meta">
          <Link to="post" params={{ title: "hello" }}>
            <span className="article__author">{this.props.author}</span>
          </Link>
          <span>&nbsp; - &nbsp;</span>
          <span className="date">{this.props.date}</span>
        </div>
        <Link to="post" params={{ title: "hello" }}>
          <div className="article__content">{this.props.title}</div>
        </Link>
      </article>
    );
  }
}