import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Layout from "./components/layout/Layout.jsx";

ReactDOM.render(
  <Router history={hashHistory}>
		<Route path="/" component={Layout}> 
			<IndexRoute component={Layout}></IndexRoute>
		</Route>
  </Router>,
  document.getElementById('app')
);