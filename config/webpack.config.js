const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require("path");
const autoprefixer = require('autoprefixer');
const NODE_ENV = process.env.NODE_ENV || 'dev';

module.exports = {
    context: __dirname + '/../src/app',
    entry: {
        app: [
            'webpack-dev-server/client?http://localhost:3000/',
            'webpack/hot/only-dev-server',
            './App.jsx'
        ]
    },

    output: {
        path: __dirname + '/../src/client',
        publicPath: '/',
        filename: 'scripts/[name].js',
        library: '[name]'
    },

    watchOptions: {
        aggregateTimeout: 100,
        poll: true
    },

    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV),
            LANG: JSON.stringify('ru')
        }),
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: 'common'
        // }),
        new ExtractTextPlugin('styles/[name].min.css', {allChunks: true, disable: NODE_ENV == 'dev'}),
        new webpack.HotModuleReplacementPlugin(),
        // new webpack.ProvidePlugin({
        //     $: "jquery",
        //     jQuery: "jquery",
        //     "window.jQuery": "jquery"
        // })
    ],

    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js', 'jsx'],
        alias: {
            vendor: path.join(__dirname, "../vendor")
        }
    },

    resolveLoader: {
        root: path.join(__dirname, "node_modules"),
        modulesDirectories: ['node_modules'],
        moduleTemplates: ['*-loader', '*'],
        extensions: ['', '.js', '.jsx']
    },

    module: {
        loaders: [
        {
            test: /\.(js|jsx)?$/,
            loaders: ['react-hot', 'babel?plugins=react-html-attrs'],
            include: [
                path.resolve(__dirname, "../src/app")
            ]
        },
        {
            test: /\.styl$/,
            loader: ExtractTextPlugin.extract('style', 'css!postcss!stylus?resolve url', {publicPath: '../'}),
            include: [path.resolve(__dirname, "../src/app")]
        }, 
        // {
        //     test: /\.(scss|sass)$/,
        //     loader: ExtractTextPlugin.extract('style', 'css?minimize!postcss!sass'),
        //     include: [path.resolve(__dirname, "../src/app")]
        // }, 
        {
            test: /\.(png|jpe?g|svg|gif)(\?.*$|$)/,
            loader: 'file?name=images/[name].[ext]',
            include: [
                path.resolve(__dirname, "../src/app"),
                path.resolve(__dirname, "../src/client/images"),
            ]
        }, 
        {
            test: /\.(ttf|eot|woff|woff2|otf)(\?.*$|$)/,
            loader: 'file?name=fonts/[name].[ext]',
            include: [
                path.resolve(__dirname, "../src/app"),
                path.resolve(__dirname, "../src/client/fonts"),
                path.resolve(__dirname, "../vendor/fonts"),
            ]
        }, 
        // {
        //     test: /\.html$/,
        //     loader: 'html',
        //     // loader: 'file?name=[name].[ext]?[hash]',
        //     include: [path.resolve(__dirname, "../src/app")]
        // }
        ],
        //noParse: [/vendor/]
    },

    postcss: [autoprefixer({browsers: ['last 2 versions']})],

    devServer: {
        host: "0.0.0.0",
        hot: true,
        port: 3000,
        contentBase: __dirname + '/../src/client'
    }
};

// if (NODE_ENV == 'prod') {
//     module.exports.plugins.push(
//         new webpack.optimize.UglifyJsPlugin({
//             compress: {
//                 warnings: false,
//                 drop_console: true,
//                 unsafe: true
//             }
//         })
//     )
// }